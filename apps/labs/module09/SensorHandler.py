'''
Created on Apr 14, 2020

@author: amans
'''
from labs.module09 import AwsIoTCore
import time


"""main function to call the sdk function and publish"""
if __name__ == '__main__':
   aws_obj= AwsIoTCore.AwsIoTCore()
   aws_obj.connection()
   while 1:
       sensor=aws_obj.sensor()
       aws_obj.shadowCreate()
       aws_obj.shadowUpdate(sensor)
       time.sleep(2)