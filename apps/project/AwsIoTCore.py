'''
Created on Apr 14, 2020

@author: amans
'''

from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTShadowClient, AWSIoTMQTTThingJobsClient
from SensorData import SensorData
from DataUtil import DataUtil
import random, logging, time
import json
from cgi import log

"""
class to update aws core iot and shadow state along with connection using sdk"""


class AwsIoTCore(object):
    '''
    classdocs
    '''

    """constructor initializes credentials required to aws iot core"""

    def __init__(self):
        '''
        Constructor
        '''
        self.sensorobj = SensorData()
        #         self.myShadowClient = AWSIoTMQTTThingJobsClient("myClientID", "myThingName")

        self.myShadowClient = AWSIoTMQTTShadowClient("myClientID")
        self.myShadowClient.configureEndpoint("a1eje6imfyhgcu-ats.iot.us-east-1.amazonaws.com", 8883)
        self.myShadowClient.configureCredentials("rootca.pem", "thing-private.pem", "thing-certificate.pem")
        print("constructor")
    #         return True
    """ function to cceate and set timers to connect to thing shadow service"""

    def connection(self):
        self.myShadowClient.configureConnectDisconnectTimeout(10)  # 10 sec
        self.myShadowClient.configureMQTTOperationTimeout(5)  # 5 sec
        self.myShadowClient.connect()
        print("connection")

    # create shadow from things id
    def shadowCreate(self):
        logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s"')

        self.myDeviceShadow = self.myShadowClient.createShadowHandlerWithName("sensor", isPersistentSubscribe=True)
        print("shadow created")
        logging.info("shadow for iot core device created")



    """create json from sensor data"""

    def sensor(self):
        logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s"')

        temp = random.uniform(15.0, 30.0)
        for i in range(1, 5):
            self.sensorobj.addvalue(temp)

        avg = self.sensorobj.getterAvg()
        json_data = DataUtil.tojsonfromSensorData(self, self.sensorobj)
        payload = {}
        payload["state"] = {}
        payload["state"]["reported"] = json.loads(json_data)
        logging.info(payload)
        #         payload=json.loads(payload)
        #         payload= {
        #         "state":{
        #             "reported":{
        #                 "temperature323232":25.5,"av":878}
        #             }
        #         }
        payload_json = json.dumps(payload)
        logging.info(type(payload_json))
        logging.info(payload_json)
        return payload_json

    # Custom  message callback for updating shadow
    def customCallback(self, client, status, token):
        print("custom callback")
        logging.basicConfig(level=logging.INFO)
        print("client custom callback",client)
        logging.info("Client %s ", client)
        rec = json.loads(client)
        print("record recieved first",rec)

        # payload_new = rec["state"]["reported"]
        avg_value = rec["state"]["reported"]["avg"]

        logging.info("=====================")
        logging.info("status %s", status)
        logging.info("=====================")
        logging.info("tokken %s", token)
        logging.info("--------------")
        logging.info("entering condition")
        if avg_value > 23:
            logging.info("entering condition high")
            print("high")
            avg_value = 22

            newpayload = rec["state"]
            newpayload["reported"]["avg"] = avg_value
            newpayload["reported"]["condition"]="HIGH Temp"

            final_payload = {}
            final_payload["state"] = newpayload
            final_payload = json.dumps(final_payload)
            print(final_payload)
            self.myDeviceShadow.shadowUpdate(final_payload, self.updateCustomCallback, 3)




    # update callback for updating thing state and shadow updation
    def updateCustomCallback(self, client, status, token):
        print("update custome callback")
        logging.basicConfig(level=logging.INFO)

        logging.info("Client %s ", client)
        rec = json.loads(client)
        logging.info("=====================")
        logging.info("status %s", status)
        logging.info("=====================")
        logging.info("tokken %s", token)
        logging.info("--------------")






    # change shadow state to update values of the shadow
    def shadowUpdate(self, payload_data):
        logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s"')

        logging.info(payload_data)
        # logging.info(type(payload_data))
        print("shadow update func")
        self.myDeviceShadow.shadowUpdate(payload_data, self.customCallback, 5)
        time.sleep(1.0)

    def shadowjson(self,normal_json):
        payload = {}
        payload["state"] = {}
        payload["state"]["reported"] = normal_json
        payload["state"]["reported"]["condition"] = "constant"
        # logging.info(normal_json)

        payload_json = json.dumps(payload)
        print("state json created")
        # logging.info(type(payload_json))
        logging.info(payload_json)
        return payload_json
