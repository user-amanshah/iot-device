'''
Created on Feb 9, 2020

@author: amanshah
'''
from datetime import datetime
import time, threading



# from labs.module04.MultiSensorAdaptor import MultiSensorAdaptor
# from labbenchstudios.common.ActuatorData import ActuatorData
from SmtpClientConnector import SMTPemailclass
from AwsIoTCore import AwsIoTCore
import logging
from mqttconnector import Mqttclientconnector
from threading import Thread

from asyncio.tasks import sleep
class Actuation(object):
    '''
    classdocs
    '''


    """
    setters and getters and initialization
    """

    # actuatorAdaptor = TempActuatorAdaptor()
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)s %(message)s"')


    def __init__(self,json_data):

        threading.Thread.__init__(self)
        self.jsondata=json_data
        '''
        Constructor
        '''

    #
    # def run(self):
    #     self.manager_temp( self.jsondata)
    #     thread2 = Thread(target=self.generate_website_thumbnail)



    def manager_temp(self):
        temp_json=self.jsondata
        average_temp=temp_json["avg"]
        average_temp=int(average_temp)
        if average_temp < 20 or average_temp > 25:
            email=SMTPemailclass()
            topic= "The temperature has suddenly changed to high percent value from average "+str(average_temp)+"to a sudden change."
            formatstring=temp_json
            time.sleep(0.6)
            email.sendemailmethod(topic, formatstring)
            logging.info("email_send")
            obj = Mqttclientconnector()
            obj.connect("on_connect")
            obj.publishMessage("test_temp","{'ACTUATION':'CHANGE'} ")






        self.aws_obj= AwsIoTCore()
        self.aws_obj.connection()

        self.aws_obj.shadowCreate()

        # print("entered manager")

        update_json= self.aws_obj.shadowjson(temp_json)
        print("state json",update_json)
        self.aws_obj.shadowUpdate(update_json)

        # min= sensorvalues.getterMin()
        """
         check if email is to be sent using avg values
        """


