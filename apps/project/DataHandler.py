'''
Created on Mar 20, 2020

@author: aman shah
'''

import  mqttconnector
from time import sleep


class Datahandler(object):
    '''
    classdocs
    '''

    def __init__(self):
        '''
        Constructor
        '''

    def mqttconnector(self):

        # MqttClientConnector instance
        connector = mqttconnector.Mqttclientconnector()
        # Connecting to broker
        connector.connect(None, None)
        #subscribing to a topic
        # connector.subscibetoTopic("test")
        connector.publishMessage('test',"hi",2)
        sleep(6)
        # unsubscribe
        # connector.unsubscibefromTopic("test")
        # disconnecting
        connector.disconnect()


