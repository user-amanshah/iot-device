'''
Created on Apr 19, 2020

@author: amans
'''


import RPi.GPIO as GPIO
import time
import SensorData
from DataUtil import DataUtil
import json,random

import logging

class Ultrasonic(object):
    '''
    classdocs
    '''

    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)s %(message)s"')
    
    def __init__(self):
        '''
        Constructor
        '''
        
        
    
    
    def calculate_dist(self):
        try:
            GPIO.setmode(GPIO.BOARD)
            #GPIO.setmode(GPIO.BCM)
    
            PIN_TRIGGER = 7
            PIN_ECHO = 11
    
            GPIO.setup(PIN_TRIGGER, GPIO.OUT)
            GPIO.setup(PIN_ECHO, GPIO.IN)
    
            GPIO.output(PIN_TRIGGER, GPIO.LOW)
    
            print("Waiting for sensor to settle")
    
            time.sleep(0.5)
    
            print("Calculating distance")
    
            GPIO.output(PIN_TRIGGER, GPIO.HIGH)
    
            time.sleep(0.00001)
    
            GPIO.output(PIN_TRIGGER, GPIO.LOW)
    
            while GPIO.input(PIN_ECHO)==0:
                pulse_start_time = time.time()
            while GPIO.input(PIN_ECHO)==1:
                pulse_end_time = time.time()
    
            pulse_duration = pulse_end_time - pulse_start_time
            distance = round(pulse_duration * 17150, 2)
            print("Distance:",distance,"cm")
            logging.info(distance)
            return distance
    
        finally:
            GPIO.cleanup()
    
    
    
    def generate_sensor(self):
        sensor = SensorData.SensorData()
        
        for i in range(1, 5):
            #dist=self.calculate_dist()
            dist = random.uniform(15.0, 30.0)
            sensor.addvalue(dist)
            time.sleep(0.5) 
        avg = sensor.getterAvg()
        print("avg door dist",avg)
        json_data = DataUtil.tojsonfromSensorData(self, sensor)
        json_data=json.loads(json_data)
        json_data["sensor"]="ultrasonic"
        json_data=json.dumps(json_data)
        logging.info(json_data)
        return json_data
        
    
    def check_door(self,Ultrasonic_json):
        
        dist=json.loads(Ultrasonic_json)
        avg_dist=dist["avg"]
        print("check distance",avg_dist)
        if avg_dist< 100:
            print("sending to gateway",avg_dist)
            
            logging.info(dist)
            logging.info("sending to gateway")
            return True,dist
        else:
            return False,dist
        
