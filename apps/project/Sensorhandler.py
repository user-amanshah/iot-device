# '''
# Created on 20-Mar-2020
#
# @author: aman hah
# '''


import paho.mqtt.client as mqttClient
import time, json, threading,sys
from Actuation import  Actuation

"""
This function deals with communication from constrianed device rpi to gateway
"""
#callback funcyion from rpi to gateway
def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print("Connected to broker")
        global Connected                #Use global variable
        Connected = True                #Signal connection
    else:
        print("Connection failed")

#callback to recived message fom constrain device
def on_message(client, userdata, message):
    recieved=str(message.payload.decode("utf-8"))
    print("Message received local mqtt: ", str(message.payload.decode("utf-8")))
    print(type(recieved))
    recieved = json.loads(recieved)
    sensor_type=recieved["sensor"]

    if sensor_type=='temperature':
        processThread =Actuation(recieved)
        processThread.manager_temp();
    # sys.exit()

# set up conection
Connected = False   #global variable for the state of the connection

broker_address= "localhost"  #Broker address
port = 1883                         #Broker port
user = "iot2020"                    #Connection username
password = "iot123"            #Connection password

client = mqttClient.Client("Python")               #create new instance
client.username_pw_set(user, password=password)    #set username and password
client.on_connect= on_connect                      #attach function to callback
client.on_message= on_message                      #attach function to callback

client.connect(broker_address, port=port)          #connect to broker

client.loop_start()        #start the loop

while Connected != True:    #Wait for connection
    time.sleep(0.1)

client.subscribe("test")
client.subscribe("test_temp")


#listen to infinite loop
try:
    while True:
        time.sleep(1)


except KeyboardInterrupt:
    print("exiting")
    client.disconnect()
    client.loop_stop()





