import boto3
import json,os,time

os.environ['AWS_PROFILE'] = "prod2"
os.environ['AWS_DEFAULT_REGION'] = "us-east-1"
client = boto3.client('iot-data', region_name='us-east-1')

while 1:
    response = client.get_thing_shadow(thingName='sensor')

    streamingBody = response["payload"]
    jsonState = json.loads(streamingBody.read())


    print(jsonState["state"]["reported"])

    time.sleep(3)
