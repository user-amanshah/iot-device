import unittest


"""
Test class for all requisite Module09 functionality.

Instructions:
1) Rename 'testSomething()' method such that 'Something' is specific to your needs; add others as needed, beginning each method with 'test...()'.
2) Add the '@Test' annotation to each new 'test...()' method you add.
3) Import the relevant modules and classes to support your tests.
4) Run this class as unit test app.
5) Include a screen shot of the report when you submit your assignment.

Please note: While some example test cases may be provided, you must write your own for the class.
"""

from labs.module09 import AwsIoTCore
import time

class Module09Test(unittest.TestCase):

	"""
	Use this to setup your tests. This is where you may want to load configuration
	information (if needed), initialize class-scoped variables, create class-scoped
	instances of complex objects, initialize any requisite connections, etc.
	"""
	"""initiialize classes and methods required"""
	def setUp(self):
		time.sleep(2)
		self.iotcore = AwsIoTCore.AwsIoTCore()
		self.sensor= AwsIoTCore.AwsIoTCore.sensor(self)
		

	"""
	Use this to tear down any allocated resources after your tests are complete. This
	is where you may want to release connections, zero out any long-term data, etc.
	"""
	def tearDown(self):
		self.iotcore=None
		

	"""
	test to find if connection to iot core is successfull
	"""
	def test_connection(self):
		self.iotcore.connection()
		
		
	"""test to check if updation is working"""
	
	def test_updation(self):
		self.iotcore.shadowUpdate(self.sensor())

if __name__ == "__main__":
	#import sys;sys.argv = ['', 'Test.testName']
	unittest.main()